package de.andsafe.vermittler;

import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;

import de.andsafe.vermittler.model.Model;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class AutomatisierteVermittlerAnlage {

  private JFrame frame;
  private JFileChooser fileChooser;

  private JLabel vpnDatenLabel;
  private JTextField vpnUsername;
  private JPasswordField vpnPassword;

  private JButton dateiButton;
  private JLabel dateiLabel;

  private JButton importButton;

  private JProgressBar progressBar;
  private JLabel progressLabel;
  private JTextArea logArea;
  private JScrollPane logPane;
  private JComboBox<String> hostList;

  private Model model = new Model();
  private Map<String,String> hosts = Map.of("PROD","https://andsafe.de/api/vermittler/vermittler/",
      "INT","https://integration.andsafe.de/api/vermittler/vermittler/",
      "DEV","https://development.andsafe.de/api/vermittler/vermittler/");

  public static void main(String[] args) {
    SwingUtilities.invokeLater(AutomatisierteVermittlerAnlage::new);
  }

  public AutomatisierteVermittlerAnlage() {
    this.getFrame().add(this.getVpnDatenLabel());
    this.getFrame().add(this.getHostList());
    this.getFrame().add(this.getVpnUsername());
    this.getFrame().add(this.getVpnPassword());

    this.getFrame().add(this.getFileChooser());

    this.getFrame().add(this.getDateiLabel());
    this.getFrame().add(this.getDateiButton());

    this.getFrame().add(this.getImportButton());

    this.getFrame().add(this.getProgressBar());
    this.getFrame().add(this.getProgressLabel());
    this.getFrame().add(this.getLogPane());

    this.getFrame().repaint();
  }

  private JFrame getFrame() {
    if (frame == null) {
      frame = new JFrame();
      frame.setSize(520, 500);
      frame.setLayout(null);
      frame.setVisible(true);
      frame.setResizable(false);
      frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      frame.setTitle("Vermittler Goes Cognito");
    }
    return frame;
  }

  private JScrollPane getLogPane() {
    if (logArea == null) {
      logArea = new JTextArea();
      logArea.setEnabled(false);
      logPane = new JScrollPane(logArea, VERTICAL_SCROLLBAR_ALWAYS, HORIZONTAL_SCROLLBAR_ALWAYS);
      logPane.setBounds(10, 170, 500, 300);

      logPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
        public void adjustmentValueChanged(AdjustmentEvent e) {
          e.getAdjustable().setValue(e.getAdjustable().getMaximum());
        }
      });
    }
    return logPane;
  }

  private JFileChooser getFileChooser() {
    if (this.fileChooser == null) {
      this.fileChooser = new JFileChooser();
    }
    return this.fileChooser;
  }

  public JLabel getVpnDatenLabel() {
    if (vpnDatenLabel == null) {
      vpnDatenLabel = new JLabel("VPN Zugangsdaten:");
      vpnDatenLabel.setBounds(10, 10, 380, 20);
    }
    return vpnDatenLabel;
  }

  public JTextField getVpnUsername() {
    if (vpnUsername == null) {
      String placeholder = "max.mueller";
      vpnUsername = new JTextField(placeholder);
      vpnUsername.setBounds(10, 40, 190, 20);
      vpnUsername.setForeground(Color.GRAY);
      vpnUsername.addFocusListener(new FocusListener() {
        @Override
        public void focusGained(FocusEvent e) {
          if (vpnUsername.getText().equals(placeholder)) {
            vpnUsername.setText("");
            vpnUsername.setForeground(Color.BLACK);
          }
        }

        @Override
        public void focusLost(FocusEvent e) {
          if (vpnUsername.getText().isEmpty()) {
            vpnUsername.setForeground(Color.GRAY);
            vpnUsername.setText(placeholder);
          }
        }
      });

    }
    return vpnUsername;
  }

  public JPasswordField getVpnPassword() {
    if (vpnPassword == null) {
      String placeholder = "made by cuby";
      vpnPassword = new JPasswordField(placeholder);
      vpnPassword.setBounds(200, 40, 190, 20);
      vpnPassword.setForeground(Color.GRAY);
      vpnPassword.addFocusListener(new FocusListener() {
        @Override
        public void focusGained(FocusEvent e) {
          if (new String(vpnPassword.getPassword()).equals(placeholder)) {
            vpnPassword.setText("");
            vpnPassword.setForeground(Color.BLACK);
          }
        }

        @Override
        public void focusLost(FocusEvent e) {
          if (new String(vpnPassword.getPassword()).isEmpty()) {
            vpnPassword.setForeground(Color.GRAY);
            vpnPassword.setText(placeholder);
          }
        }
      });
    }
    return vpnPassword;
  }

  private JComboBox<String> getHostList() {
    if(hostList == null) {
      hostList = new JComboBox<>(new String[]{"PROD", "INT", "DEV"});
      hostList.setSelectedIndex(2);
      hostList.setBounds(390, 40, 130, 20);
    }
    return hostList;
  }

  private String getHost() {
    final String item = hostList.getItemAt(hostList.getSelectedIndex());
    return hosts.get(item);
  }

  private JButton getDateiButton() {
    if (this.dateiButton == null) {
      this.dateiButton = new JButton();
      this.dateiButton.setBounds(430, 80, 80, 20);
      this.dateiButton.setText(Model.AUSWAEHLEN);
      this.dateiButton.addActionListener(this::waehleDateiAus);
    }
    return this.dateiButton;
  }

  public JProgressBar getProgressBar() {
    if (progressBar == null) {
      progressBar = new JProgressBar();
      progressBar.setBounds(10, 140, 420, 20);
      progressBar.setMinimum(0);
      progressBar.setMaximum(100);
      progressBar.setValue(0);
    }
    return progressBar;
  }

  public JLabel getProgressLabel() {
    if (progressLabel == null) {
      progressLabel = new JLabel();
      progressLabel.setBounds(430, 140, 80, 20);
      progressLabel.setText("0%");
      progressLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    }
    return progressLabel;
  }

  private JLabel getDateiLabel() {
    if (this.dateiLabel == null) {
      this.dateiLabel = new JLabel();
      this.dateiLabel.setBounds(10, 80, 420, 20);
      this.dateiLabel.setText(Model.KEIN_DATEI_AUSGEWAEHLT);
    }
    return this.dateiLabel;
  }

  public JButton getImportButton() {
    if (importButton == null) {
      this.importButton = new JButton();
      this.importButton.setText(Model.IMPORTIEREN);
      this.importButton.setBounds(10, 110, 500, 20);
      this.importButton.setEnabled(false);
      this.importButton.addActionListener(getImportActionListener());
    }
    return importButton;
  }

  private ActionListener getImportActionListener() {
    return e -> {
      refreshProgressbar(0);
      new Thread(() -> {
        importiereVermittler();
        model.getVermittler().forEach(this::registriere);
      }).start();
      importButton.setEnabled(false);
    };
  }

  private void registriere(HashMap<String, String> v) {
    if (("j").equalsIgnoreCase(v.get(this.model.COGNITO))) {
      HttpResponse<String> result = registriereVermittler(v);
      if (result == null || result.statusCode() != 201) {
        final String message =
            "Vermittler "
                + v.get(Model.VERMITTLERNUMMER)
                + " konnte nicht angelegt werden (Status-Code: "
                + (result == null ? -1 : (result.statusCode() + " " + result.body()))
                + ")";
        logArea.append("\n");
        logArea.append(message);
      } else {
        logArea.append("\nVermittler " + v.get(Model.VERMITTLERNUMMER) + " ist erfolreich angelegt worden");
      }
    } else {
      final String message =
              "Vermittler "
                      + v.get(Model.VERMITTLERNUMMER)
                      + " erhält keinen Cognito Account.";
      logArea.append("\n");
      logArea.append(message);
    }
    refreshProgressbar(model.getVermittler().indexOf(v) + 1);
  }

  public void waehleDateiAus(ActionEvent e) {
    JFileChooser fc = new JFileChooser();
    int i = fc.showOpenDialog(this.getFrame());
    if (i == JFileChooser.APPROVE_OPTION) {
      File f = fc.getSelectedFile();
      String filepath = f.getPath();
      this.getDateiLabel().setText(filepath);
      this.getImportButton().setEnabled(true);
      refreshProgressbar(0);
    } else {
      this.getDateiLabel().setText(Model.KEIN_DATEI_AUSGEWAEHLT);
      this.getImportButton().setEnabled(false);
      refreshProgressbar(0);
    }
  }

  private void importiereVermittler() {
    File datei = new File(this.getDateiLabel().getText());
    if (datei.canRead() && !datei.isDirectory() && datei.getName().endsWith(".csv")) {
      this.model.getIndex().clear();
      this.model.getVermittler().clear();
      try (BufferedReader br = new BufferedReader(new FileReader(this.getDateiLabel().getText()))) {
        String zeile = "";
        int zeilennummer = 1;
        while ((zeile = br.readLine()) != null) {
          if (zeilennummer == 1) {
            this.model.baueIndex(zeile);
            this.logArea.append("\n\nIndex der Datei " + datei.getAbsolutePath() + " erstellt.\n");
          } else {
            fuelleDaten(zeile);
          }
          zeilennummer++;
        }
        this.logArea.append("Datei " + datei.getAbsolutePath() + " eingelesen.\n");
      } catch (Exception e) {
        errorMessage(datei, e.getMessage());
      }
    } else {
      errorMessage(datei, null);
    }
  }

  private void fuelleDaten(final String zeile) {
    try {
      this.model.fuelleIndex(zeile);
    }catch (IllegalArgumentException e) {
      logArea.append("\n");
      logArea.append("Zeile ist fehlerhaft: " + zeile);
    }
  }

  private void errorMessage(final File datei, String reason) {
    String message = "Die Datei " + datei.getAbsolutePath() + " kann nicht verarbeitet werden.";
    if (reason != null) {
      message += " Grund: " + reason;
    }
    JOptionPane.showMessageDialog(getFrame(), message, "Fehler", JOptionPane.ERROR_MESSAGE);
  }

  private HttpResponse<String> registriereVermittler(HashMap<String, String> v) {
    String name = this.getVpnUsername().getText();
    String password = new String(this.getVpnPassword().getPassword());

    String authString = name + ":" + password;
    String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());

    HttpClient client = HttpClient.newHttpClient();
    String url = getHost() + v.get(Model.EMAIL).toLowerCase();

    String body =
        "{\n"
            + "  \"vorname\": \"" + v.get(Model.VORNAME) + "\",\n"
            + "  \"nachname\": \"" + v.get(Model.NACHNAME) + "\",\n"
            + "  \"vermittlernummer\": \"" + v.get(Model.VERMITTLERNUMMER) + "\",\n"
            + "  \"rollen\": {\n"
            + "    \"hauptvermittlersicht\": " + "j"
            .equalsIgnoreCase(v.get(Model.HAUPTVERMITTLERSICHTRECHT)) + "\n"
            + "  }\n"
            + "}";


    HttpRequest request =
        HttpRequest.newBuilder()
            .uri(URI.create(url))
            .POST(HttpRequest.BodyPublishers.ofString(body))
            .header("Authorization", "Basic " + authStringEnc)
            .header("Content-Type", "application/json")
            .build();

    HttpResponse<String> response = null;

    try {
        response = client.send(request,
                HttpResponse.BodyHandlers.ofString());

    } catch (Exception e) {
      System.out.println("Http Fehler: " + e.getMessage());
      }

    return response;

  }

  private void refreshProgressbar(int index) {
    SwingUtilities.invokeLater(() -> {
      int percent = 0;
      if (!model.getVermittler().isEmpty()) {
        percent = (int) ((float)index / model.getVermittler().size() * 100);
      }
      this.getProgressBar().setValue(percent);
      this.getProgressLabel().setText(percent + "%");
    });
  }
}
