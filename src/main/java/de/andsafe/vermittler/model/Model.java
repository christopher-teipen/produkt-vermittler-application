package de.andsafe.vermittler.model;

import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Model {

    public static final String KEIN_DATEI_AUSGEWAEHLT = "Keine Datei ausgewählt";
    public static final String IMPORTIEREN = "importieren";
    public static final String AUSWAEHLEN = "...";
    public static final String VERMITTLERNUMMER = "Vermittlernummer";
    public static final String EMAIL = "E-Mail";
    public static final String VORNAME = "Vorname";
    public static final String NACHNAME = "Name";
    public static final String HAUPTVERMITTLERSICHTRECHT = "Hauptvermittlersicht";
    public static final String COGNITO = "Cognito";

    private List<String> index = new ArrayList<>();
    private List<HashMap<String, String>> vermittler = new ArrayList<>();

    public List<String> getIndex() {
        return index;
    }

    public List<HashMap<String, String>> getVermittler() {
        return vermittler;
    }

    public void fuelleIndex(String zeile) {
      String[] splittedZeile = zeile.split(";",-1);
      if (splittedZeile.length == getIndex().size()) {
        HashMap<String, String> content = new HashMap<>();
        index
            .forEach(i -> content.put(i, splittedZeile[index.indexOf(i)]));
        vermittler.add(content);
      } else {
        throw new IllegalArgumentException();
      }
    }

    public void baueIndex(String zeile) {
        index.addAll(Arrays.asList(zeile.split(";")));
        index.stream().map(a -> a.trim());

        String message = "Spalte \"%s\" nicht vorhanden (Wert: %s)";

        if(!index.contains(COGNITO)) {
            throw new IllegalArgumentException(String.format(message, COGNITO, index.toString()));
        }

        if(!index.contains(VERMITTLERNUMMER)) {
            throw new IllegalArgumentException(String.format(message, VERMITTLERNUMMER, index.toString()));
        }

        if(!index.contains(EMAIL)) {
            throw new IllegalArgumentException(String.format(message, EMAIL, index.toString()));
        }

        if(!index.contains(VORNAME)) {
            throw new IllegalArgumentException(String.format(message, VORNAME, index.toString()));
        }

        if(!index.contains(NACHNAME)) {
            throw new IllegalArgumentException(String.format(message, NACHNAME, index.toString()));
        }

        if(!index.contains(HAUPTVERMITTLERSICHTRECHT)) {
            throw new IllegalArgumentException(String.format(message, HAUPTVERMITTLERSICHTRECHT, index.toString()));
        }
    }
}
